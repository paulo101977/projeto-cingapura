# Projeto Cingapura

##URL para testes:
http://mgmtposloginprd.telemar/portal/site/MinhaOi/ProcessarLogin?CPF=07899823315


### instalando localmente:
na pasta do projeto, rodar 'npm install'

### construindo o Projeto
rode 'gulp build:dist'.

Os arquivos de templates estarão compilados dentro de dist/route

###Como testar:

rode 'gulp serve' para rodar localmente.

O Projeto estará rodando em localhost:8080

###Rotas locais para testes:

1 - Histórico

route/historico_de_solicitacoes.html
route/historico_de_solicitacoes_erro_mostrar_mais.html
route/historico_de_solicitacoes_filtro_Erro.html
route/historico_de_solicitacoes_historico_vazio.html
route/historico_de_solicitacoes_loading.html
route/historico_de_solicitacoes_maximo_carregado.html
route/historico_solicitacao_sem_resultado.html
route/historico_solicitacoes_sucesso.html

2- Compra de Pacotes
route/compra_de_pacotes_erro.html
route/compraPacote.html
route/compraPacoteConfirma.html
route/compraPacoteEscolha.html
route/compraPacoteSucesso.html

3 - Home do Produto

route/home_do_produto.html
route/home_do_produto_erro_ao_carregar.html

4 - Home minha oi

route/home-minha-oi.html
route/home-produto-sva-erro.html
route/home-produto-sva.html

5 - Transferências

route/transferencia-saldo.html
route/transferencia-1-ator.html
route/transferencia-ator-nao-selecionado.html
route/transferencia-atores-desabilitados.html
route/transferencia-erro-grave.html
route/transferencia-erro.html
route/transferencia-escolher-3-atores.html
route/transferencia-escolher-5-atores.html
route/transferencia-escolher-atores.html
route/transferencia-saldo-ator-desabilitado.html

6 - Redefinição:

route/redistribuicao_de_saldo.html
route/redistribuicao_de_saldo_agendada.html
route/redistribuicao_de_saldo_redefinindo.html
route/redistribuicao_de_saldo_1_ator.html
route/redistribuicao_de_saldo_erro.html
route/redistribuicao_de_saldo_sucesso.html
