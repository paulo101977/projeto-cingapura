$(document).ready(function(){

  function formatNumberComma (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }

  function formatNumber(num){
     num = num/1000;

    if(num == parseInt(num)){
      return formatNumberComma(parseInt(num));
    }

    return formatNumberComma(num.toFixed(2));
  }


  //server side values
  var initialUserMapValues = {
    'minValue': 10, //min value
    'usersInfo': [
      //first
      {
        name: 'Marianna',
        phone: '(21) 98712-0921',
        value: 10,
        avulso: 2000,
        bloqueado: true
      },
      //second
      {
        name: 'Marcela',
        phone: '(21) 92812-0992',
        value: 2100,
        avulso: 0,
        bloqueado: true
      },
      //third
      {
        name: 'Ludmilla',
        phone: '(21) 98712-0921',
        value: 10,
        avulso: 0,
        bloqueado: true
      },
      //fourth
      {
        name: 'Henrique',
        phone: '(21) 99912-2912',
        value: 1800,
        avulso: 0,
        bloqueado: true
      },
      //fifth
      {
        name: 'Fernando',
        phone: '(21) 98712-0921',
        value: 4100,
        avulso: 2000,
        bloqueado: true
      }
    ]
  }

var currentMapValues = [];

var setInitialValues = function(values){
  var usersInfo = initialUserMapValues.usersInfo;
  var minValue = initialUserMapValues.minValue;
  var first = usersInfo[0];
  var second = usersInfo[1];
  var third = usersInfo[2];
  var fourth = usersInfo[3];
  var fifth = usersInfo[4];


  currentMapValues.push(first.value)//[0]
  currentMapValues.push(second.value)//[1]
  currentMapValues.push(third.value)//[2]
  currentMapValues.push(fourth.value)//[4]
  currentMapValues.push(fifth.value)//[5]
  var firstFormatedAvulso = formatNumber(first.value);
  var secondFormatedAvulso = formatNumber(second.value);
  var thirdFormatedAvulso = formatNumber(third.value);
  var fourthFormatedAvulso = formatNumber(fourth.value);
  var fifthFormatedAvulso = formatNumber(fifth.value);


  //first
  $('.first-name').text(first.name)
  $('.first-phone').text(first.phone)
  $('.first-value').text(formatNumber(first.value));

  if(first.avulso) {
    $('.first-avulso-value').text("+" + formatNumber(first.avulso) +" GB avulso")
  }


  if(firstFormatedAvulso < 1) {
    $('.first-value').addClass("low-qty");
    $('.first-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");

  }

  $('.second-name').text(second.name)
  $('.second-phone').text(second.phone)
  $('.second-value').text(formatNumber(second.value))

  if(second.avulso){
    $('.second-avulso-value').text("+" + formatNumber(second.avulso) +" GB avulso")
  }

  if(secondFormatedAvulso < 1) {
    $('.second-value').addClass("low-qty");
    $('.second-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");

  }

  $('.third-name').text(third.name)
  $('.third-phone').text(third.phone)
  $('.third-value').text(formatNumber(third.value))

  if(third.avulso){
    $('.third-avulso-value').text("+" + formatNumber(third.avulso) +" GB avulso")
  }

  if(thirdFormatedAvulso < 1) {
    $('.third-value').addClass("low-qty");
    $('.third-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");

  }

  $('.fourth-name').text(fourth.name)
  $('.fourth-phone').text(fourth.phone)
  $('.fourth-value').text(formatNumber(fourth.value))

  if(fourth.avulso){
    $('.fourth-avulso-value').text("+" + formatNumber(fourth.avulso) +" GB avulso")
  }

  if(fourthFormatedAvulso < 1) {
    $('.fourth-value').addClass("low-qty");
    $('.fourth-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");

  }

  $('.fifth-name').text(fifth.name)
  $('.fifth-phone').text(fifth.phone)
  $('.fifth-value').text(formatNumber(fifth.value))

  if(fifth.avulso){
    $('.fifth-avulso-value').text("+" + formatNumber(fifth.avulso) +" GB avulso")
  }
  if(fifthFormatedAvulso < 1) {
    $('.fifth-value').addClass("low-qty");
    $('.fifth-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");

  }
}

function countChecked() {
  var n = $( "input:checked" ).length;

  if (n <= 1) {

    $(".warn-message-info.selecionar-2-atores").removeClass("dipnone");
  } else {
    $(".warn-message-info.selecionar-2-atores").addClass("dipnone");
  }
}

function removeMsgCheck() {
  $( "input[type=checkbox]" ).on( "click", function() {
    $(".warn-message-info.selecionar-2-atores").addClass("dipnone");
  });
}


function disableMinValue() {
  $(":checkbox").change(function(){
    var checkMin = $(":checkbox[value='1']:not(:checked)");
    if ($(":checkbox[value='1']:checked").length == 1) {
      $(checkMin).addClass("dipnone-second");
      checkMin.parent(".panel").click(function () {
         $('.dois-minimos').removeClass('dipnone');
      })
     }
    else {
        $(checkMin).removeClass("dipnone-second");
    }
    $('.dois-minimos').addClass('dipnone');
  })
}

function changeClassCheck() {
  var lastChecked;
  var $checks = $('input:checkbox').click(function(e) {

      var numChecked = $checks.filter(':checked').length;

      if (numChecked > 2) {
          lastChecked.checked = false;
        $(lastChecked).parent().removeClass('active');
        $(lastChecked).siblings().removeClass('active');
        $(lastChecked).siblings(".panel-heading").children('.panel-heading__itemNumber').removeClass('white-color-opacity');
        $(lastChecked).siblings(".panel-body").children(".panel-heading__itemQty").children().removeClass('white-color');
        $(lastChecked).siblings(".panel-body").children(".panel-heading__itemQty").children().siblings('.panel-heading__itemQty--lower').removeClass('white-color');
        $(lastChecked).siblings(".panel-body").children(".avulso-panel").children('.panel-heading__itemQtyAvulso').removeClass('white-color-opacity');
      }
      lastChecked = this;

      addClassCheck();

  });
}

function addClassCheck() {
  $('.add-ator').change(function() {
  var $check = $(this),
  $div = $check.parent();
  if ($check.prop('checked')) {
    $div.addClass('active');
    $check.siblings().addClass('active');
    $check.siblings(".panel-heading").children('.panel-heading__itemNumber').addClass('white-color-opacity');
    $check.siblings(".panel-body").children(".panel-heading__itemQty").children().addClass('white-color');
    $check.siblings(".panel-body").children(".panel-heading__itemQty").children().siblings('.panel-heading__itemQty--lower').addClass('white-color');
    $check.siblings(".panel-body").children(".avulso-panel").children('.panel-heading__itemQtyAvulso').addClass('white-color-opacity');
  } else {
      $div.removeClass('active');
      $check.siblings().removeClass('active');
      $check.siblings(".panel-heading").children('.panel-heading__itemNumber').removeClass('white-color-opacity');
      $check.siblings(".panel-body").children(".panel-heading__itemQty").children().removeClass('white-color');
      $check.siblings(".panel-body").children(".panel-heading__itemQty").children().siblings('.panel-heading__itemQty--lower').removeClass('white-color');
      $check.siblings(".panel-body").children(".avulso-panel").children('.panel-heading__itemQtyAvulso').removeClass('white-color-opacity');
    }


  })
}


$( ".btn-transferir-saldo" ).on( "click", function() {
  countChecked();
  removeMsgCheck();
});

setInitialValues(initialUserMapValues);
disableMinValue();
changeClassCheck();
})
