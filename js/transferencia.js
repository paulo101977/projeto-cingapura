//var minValue = 10; //10MB

function formatNumberComma (num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function formatNumber(num){
   num = num/1000;

  if(num == parseInt(num)){
    return formatNumberComma(parseInt(num));
  }

  return formatNumberComma(num.toFixed(2));
}

//server side values
var initialValuesUserbar = {
  'minValue': 10, //min value
  'usersInfo': [
    //left
    {
      name: 'Marianna',
      phone: '(21) 98712-0921',
      value: 20,
      avulso: 4000,
      bloqueado: true
    },
    //right
    {
      name: 'Ludmilla',
      phone: '(21) 98001-1977',
      value: 3000,
      avulso: 0,
      bloqueado: true
    }
  ]
}

var currentValuesUserbar = [];

var setInitialBarValues = function(values){
  //console.log(values)
  var usersInfo = values.usersInfo;
  var left = usersInfo[0];
  var right = usersInfo[1];

  currentValuesUserbar.push(left.value)//[0]
  currentValuesUserbar.push(right.value)//[1]

  //$('.warn-message-info.ator-desabilitado').hide();
  $('.blocknumber-left').hide();
  $('.blocknumber-right').hide();

  if(left.bloqueado) $('.blocknumber-left').show();
  if(right.bloqueado) $('.blocknumber-right').show();

  //left
  $('.left-name').text(left.name)
  $('.left-phone').text(left.phone)
  $('.left-value').text(formatNumber(left.value));

  if(left.avulso) {
    $('.left-avulso-value').text("+" + formatNumber(left.avulso) +" GB avulso")
  }

  $('.right-name').text(right.name)
  $('.right-phone').text(right.phone)
  $('.right-value').text(formatNumber(right.value))

  if(right.avulso){
    $('.right-avulso-value').text("+" + formatNumber(right.avulso) +" GB avulso")
  }
}

function clearErrorAdd(){
  $('.left-container-avulso').removeClass('nactive')
  $('.right-container-avulso').removeClass('nactive')
  $('.warn-message-info.ator-desabilitado').removeClass('active');
  $('.warn-message-info.ator-desabilitado').addClass('nactive');
}

function showWarningMessage(className){
  $(className).addClass('nactive')
  $('.warn-message-info.ator-desabilitado').removeClass('nactive');
  $('.warn-message-info.ator-desabilitado').addClass('active');
  $('.warn-message-info.ator-desabilitado .phone').text(initialValuesUserbar.usersInfo[0].phone);
}

function incrementRight(hasAnimation){
  if(currentValuesUserbar[0] > initialValuesUserbar.minValue){
    currentValuesUserbar[0] -= 10;
    currentValuesUserbar[1] += 10;
    $('.left-value').text(formatNumber(currentValuesUserbar[0]))
    $('.right-value').text(formatNumber(currentValuesUserbar[1]))

    clearErrorAdd();

    $('.right-container-avulso').addClass('animate')

    if(hasAnimation){
      setTimeout(function(){
        $('.right-container-avulso').removeClass('animate')
      }, 300)
    }
  } else {
    $('.right-container-avulso').removeClass('animate')
    showWarningMessage('.left-container-avulso')
    return true;
  }
}

function incrementLeft(hasAnimation){
  if(currentValuesUserbar[1] > initialValuesUserbar.minValue){
    currentValuesUserbar[1] -= 10;
    currentValuesUserbar[0] += 10;
    $('.left-value').text(formatNumber(currentValuesUserbar[0]))
    $('.right-value').text(formatNumber(currentValuesUserbar[1]))

    clearErrorAdd();

    $('.left-container-avulso').addClass('animate')

    if(hasAnimation){
      setTimeout(function(){
        $('.left-container-avulso').removeClass('animate')
      }, 300)
    }
  } else {
    $('.left-container-avulso').removeClass('animate')
    showWarningMessage('.right-container-avulso')
    return true;
  }
}

//document ready
$(document).ready(function(){

  var userEventClickId = 0;
  var incrementTimeInterval = 200;
  var tooltipUserInfo = 'Em caso de dúvidas, ligue <b>*144</b> do seu Oi ou'
                        + ' <b>1057</b> de qualquer telefone.';

  /*$('.tooltip__blocknumber').addClass('teste-opacity');

  $('.panel-heading__blockNumber').hover(function(){
    $('.tooltip__blocknumber').toggleClass('teste-opacity');
  });*/

  $('[data-toggle="tooltip-user-left"]')
    .tooltip({html: true, title: tooltipUserInfo})


  $('[data-toggle="tooltip-user-right"]')
      .tooltip({html: true, title: tooltipUserInfo})

  $('.btn-right').click(function(event){
    event.preventDefault();

    incrementRight(true);
  })
  .on('mousedown', function(event) {
      event.stopPropagation();
      event.preventDefault();

      userEventClickId = setInterval(function(){
        if(incrementRight()) {
          clearInterval(userEventClickId);
          return;
        }
      }, 150);
  })
  .on('mouseup mouseleave', function(event) {
      event.stopPropagation();
      event.preventDefault();

      clearInterval(userEventClickId);
      $('.right-container-avulso').removeClass('animate')
  });

  $('.btn-left').click(function(event){
    event.preventDefault();

    incrementLeft(true);
  })
  .on('mousedown', function(event) {
      event.stopPropagation();
      event.preventDefault();

      userEventClickId = setInterval(function(){
        if(incrementLeft()) {
          clearInterval(userEventClickId);
          return;
        }
      }, 150);
  })
  .on('mouseup mouseleave', function(event) {
      event.stopPropagation();
      event.preventDefault();

      clearInterval(userEventClickId);

      $('.left-container-avulso').removeClass('animate')
  });

  //set initial
  setInitialBarValues(initialValuesUserbar);
})
