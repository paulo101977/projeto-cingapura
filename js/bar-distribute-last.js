

//from server
var dataUser = [
  Math.pow(2,30) * 4, //4GB
  Math.pow(2,30) * 6, //6GB
  Math.pow(2,30) * 10, //10GB
  Math.pow(2,20) * 100, //100MB
  Math.pow(2,30) * 19 + Math.pow(2,20) * 900 //19.9GB
];//initial values
var phones = [
              "(21) 92812-0991" ,
              "(21) 92812-0993" ,
              "(21) 92812-0994" ,
              "(21) 92812-0995" ,
              "(21) 92812-0992"
            ]
var dataMax = Math.pow(2, 30) * 40;//40GB

//globals values
var itemSize = 18;//border
var beforeValue = 0;
var previusValue = [];
var graphicStep = 100; //100MB
var colors = ['#F8562C' , '#9F2AFF' , '#D82482' , '#FFD700' , '#00baf7' , '#00BAF7'];
var componentWidth = 0;

var currentUserValues = [];//dataUser.slice();//current data
var originalValues = [];

//converte para um valor amigável ao usuário
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

function trimmer(size, prefix){
  size = size.replace(prefix , '');
  size = size.trim();
  return size;
}

function convertSizeToNumberDisplayable(value){
  var number = 0;
  var intValue = 0;
  var fracional = 0;

  if(value == 0 || value === '0') return 0;

  if(value.indexOf("MB") !== -1){
    value = trimmer(value, "MB");
    intValue = parseInt(value/100);
    fracional = parseInt(value - intValue*100);
    number = (intValue + fracional)*100;
  }

  if(value.indexOf("GB") !== -1){
    value = trimmer(value, "GB");
    intValue = parseInt(value);
    fracional = value - intValue;
    number = (intValue + fracional) * 1000;
  }

  return number;
}

function getDirection(index , value){
  if(previusValue[index] > value){
    return 'left';
  }

  if(previusValue[index] < value){
    return 'right';
  }

  return '';
}

function initModal(values){
  if(values){
    if(typeof values.data != "undefined"
        && typeof values.phones != "undefined"
        && typeof values.max != "undefined"){

        var barContainer = $('#my-slider-fim');
        var infoContainer = $('.box__redistribuir__modal');
        var barWidth = 684;

        //console.log('barWidth', barWidth)

        values.data.forEach(function(item, index){
          var item = +item;
          var tooltip = "tooltips--bottom";

          if(index % 2 == 1) tooltip = "tooltips--top";

          var comp = '<div class="color-' + (index + 1) + ' comp-bar-item">'
            + '<a href="#" data-tooltip="' + getFormatedValue(item)
                      + '" class="' + tooltip +'">'
            +  '<span>' + getFormatedValue(item) + '</span></a>'
            +  '<div class="text">' + getFormatedValue(item) + '</div>'
            + '</div>'

          comp = $(comp);
          barContainer.append(comp)
          comp.width(item/values.max * barWidth);

          var itemInfo =
            '<div class="box__redistribuir color-' + (index + 1) + '">'
              + '<p class="cor__first">' + values.phones[index] + '</p>'
              + '<span class="cor__first">'
                    + getFormatedValue(item) + '</span>'
            + '</div>'

          infoContainer.append($(itemInfo));

        })
    }

    $('#myModal').on('show.bs.modal', function (e) {

      var position = 0;

      $('#my-slider-fim .comp-bar-item').each(function(index){
        var containerWidth = $(this).width();
        var comp = $('#my-slider-fim .color-' + (index + 1) + ' a');

        //to mesure the width, temporary append to body
        var tmpbtn = $('#my-slider-fim .color-' + (index + 1))
            .clone()
            .appendTo( "body" )
            .css({'display':'block', 'visibility': 'hidden'});
        tmpbtn
            .wrap('<div id="my-slider-fim"></div>')
            .wrap('<div class="redistribuicao__modal"></div>')
         var theWidth = tmpbtn.find('span').width();
         //var theWidth = tmpbtn.find('span').w();


        comp.css({left: position + containerWidth/2 - theWidth/2})
        position += containerWidth + 9; //width + padding/2 + border/2

        //more paddint
        if(theWidth + 8 > containerWidth){
          $(this).find('.text').css({'visibility': 'hidden'})
        } else {
          comp.css({'visibility': 'hidden'})
        }

        //remove temporary component
        tmpbtn.remove();
      })
    })
  }
}

function initBarRedistribuicaoAgendada(values){
  if(values){
    if(typeof values.data != "undefined"
        && typeof values.phones != "undefined"
        && typeof values.max != "undefined"){


      var parentBar = $("#my-slider-ini");
      var boxContainer = $('.boxes__redistribuir__without_modal');
      var parentWidth = parentBar.width();

      $('.max-value-agendada').text(getFormatedValue(values.max));

      values.data.forEach(function(item, index){

        var comp = $("<div class='color-"
                        + (+index + 1) + "'>"
                        + getFormatedValue(item)
                        + "</div>");
        parentBar.append(comp);
        comp.width(item/values.max * parentWidth);

        var box = '<div class="box__redistribuir color-' + (+index + 1) + '">'
                    + '<p class="cor__first">' + phones[index] +'</p>'
                    + '<span class="cor__first">'
                    + getFormatedValue(item) +'</span>'
                  '</div>';
        box = $(box);
        boxContainer.append(box);
      })
    }
  }
}




function checkIfHasCollision(current,before){

  var  value = parseFloat(current.val());
  var beforeValue =   parseFloat(before.val()) ;

  //first or last
  if(isNaN(value) || isNaN(beforeValue)) return false;

  if(value + 0 > beforeValue ){
    //console.log('has collision')
    return true;
  }
  //console.log('dont has collision')
  return false;
}

function getSinbling(index){
  return $('#rangeMultiple' + index)
}


function getBGSegmentColor(initial, final, color){
  return color + ' ' + initial + '% ,' + color + ' ' + final + '%';
}

function changePositions($__direction , $__index, $object){
  //console.log($__direction)
  var $__sinbling = null;

  if($__direction == 'right'){
    var $__nextIndex = parseInt($__index) + 1;
    $__sinbling = getSinbling($__nextIndex);
    var __collision = checkIfHasCollision($object,$__sinbling)

    if(__collision){
      var $__sinblingValue = parseInt($object.val()) + 0;
      //console.log($__sinblingValue)
      $__sinbling.val($__sinblingValue)
      $__sinbling.change();
    }
  } else {
    var $__beforeIndex = $__index - 1;
    $__sinbling = getSinbling($__beforeIndex);
    var __collision = checkIfHasCollision($__sinbling,$object)

    if(__collision){
      var $__sinblingValue = parseInt($object.val()) - 0;
      $__sinbling.val($__sinblingValue);
      $__sinbling.change();
    }
  }

  //if($__sinbling) $__sinbling.change();
}

function getLabel(index){
  return '<div class="label-item" id="label-item' + index + '">'
            + '<div class="label-number">' + getFormatedValue(currentUserValues[index]) + '</div>'
          +'</div>'
}

function getTooltip($index){

  var arrowClass = $index % 2 == 0 ? 'up' : 'down';

  return '<div class="c-tooltip-info-redis '
            + arrowClass +'" id="tooltip-info-redis' + $index + '">'
            + '<div class="arrow"></div>'
            + '<div class="content">' + getFormatedValue(currentUserValues[$index]) + '</div>'
          + '</div>'

}

function appendBox(phonesNumber){
  var container = $('.c-box__redistribuir');
  var originalBox = $('.c-box__redistribuir .box__redistribuir');
  //var originalBoxClone = originalBox.clone();

  currentUserValues.forEach(function(value, index){
    //console.log('index' , index)
    if(index != 0) {
      var box = originalBox.clone()
      box.addClass('color-' + (index + 1))
      box.find('span').attr('id', 'value-box-' + index)
      box.find('.c-phone').text(phonesNumber[index]);
      container.append(box)
    }
  })

  originalBox.find('span').attr('id', 'value-box-0')
  originalBox.find('.c-phone').text(phonesNumber[0]);
  originalBox.addClass('color-1')
}

function appendLabelsAndTooltip(maxValue){
  var $__labelContainer = $('.c-label-container');

  $('input[type=range][multiple]').each(function(){
    var $__current = $(this);
    var $__value = parseInt($__current.val());
    var $__index = parseInt($__current.attr('id').split('rangeMultiple')[1]);

    var __labelComp = $(getLabel($__index,$__value));
    var __tooltip = $(getTooltip($__index))

    $__labelContainer.append(__labelComp);
    $__labelContainer.append(__tooltip);

    if($__index == currentUserValues.length - 2){
      //last
      var lastIndex = $__index + 1;
      var __labelLast = $(getLabel(lastIndex,parseInt(maxValue - $__value)));
      var __lastTooltip = $(getTooltip(lastIndex));

      $__labelContainer.append(__labelLast);
      $__labelContainer.append(__lastTooltip);
    }

  })
}

function setVisibleInfo(posX , posBefore, __labelComp, __tooltip, itemSize, index){
  if((posX - posBefore < __labelComp.width()) && index == 0){
    __labelComp.hide();
    __tooltip.show();
  }
  else if(posX - posBefore < __labelComp.width() + itemSize){
    __labelComp.hide();
    __tooltip.show();
  }
  else {
    __labelComp.show();
    __tooltip.hide();
  }
}

function updatePositionLabelsAndTooltips(maxValue){

  $('input[type=range][multiple]').each(function(){
    var $__current = $(this);
    var $__value = parseInt($__current.val());
    var $__index = parseInt($__current.attr('id').split('rangeMultiple')[1]);
    var $__before = $('#rangeMultiple' + ($__index - 1));

    //var __value = currentUserValues[$__index];

    //reference
    var __labelComp = $('#label-item' + $__index);
    var __tooltip = $('#tooltip-info-redis' + $__index);

    var $__beforeValue = parseInt($__before.val());

    if(isNaN($__beforeValue)) $__beforeValue = 0;

    //ok
    var relativeCompWidth =  componentWidth - (itemSize*(currentUserValues.length - 1));
    //relativeCompWidth = relativeCompWidth - itemSize;

    //margin + relative width
    var posX = 0;

    if($__index == 0){
      var relativeValuePos = $__value*relativeCompWidth/maxValue;
      posX = (relativeValuePos - __labelComp.width())/2;

      //background
      $('.range-bg-' + $__index).width(relativeValuePos)

      setVisibleInfo(relativeValuePos , 0, __labelComp, __tooltip, itemSize, $__index)
      __labelComp.css({left: posX})

      posX = ((relativeValuePos - itemSize/2) - (__tooltip.width() + 2))/2;

      __tooltip.css({left: posX})

      //console.log('first')
      //two values
      if(currentUserValues.length == 2){
        //last
        var posLast = componentWidth;
        var lastIndex = $__index + 1;
        var __labelLast = $('#label-item' + lastIndex);
        var __lastTooltip = $('#tooltip-info-redis' + lastIndex);

        posX = itemSize*(lastIndex - 1)+ $__value*relativeCompWidth/maxValue;

        //background
        $('.range-bg-' + lastIndex).width(posLast - posX)
        setVisibleInfo(posLast , posX, __labelLast, __lastTooltip, itemSize, lastIndex);

        relativeValuePos = posX + (posLast - posX)/2;

        posX = relativeValuePos + itemSize/2 - __labelComp.width()/2;

        __labelLast.css({left:posX})

        posX = relativeValuePos - (__lastTooltip.width() + 2)/2;

        __lastTooltip.css({left:posX})
      }
    }
    else if($__index == currentUserValues.length - 2){
      //before last
      posX = itemSize*$__index + $__value*relativeCompWidth/maxValue;
      var posBefore = (itemSize*($__index - 1)) + ($__beforeValue*relativeCompWidth/maxValue);
      var __tooltip = $('#tooltip-info-redis' + $__index);
      var relativeValuePos =  posBefore + (posX - posBefore)/2;
      //background
      $('.range-bg-' + $__index).width(posX - posBefore)

      setVisibleInfo(posX , posBefore, __labelComp, __tooltip, itemSize, $__index);

      posX = relativeValuePos + itemSize/2 - __labelComp.width()/2;
      __labelComp.css({left: posX})

      posX = relativeValuePos - (__tooltip.width() + 2)/2;
      __tooltip.css({left: posX})

      //last
      var posLast = componentWidth;
      var lastIndex = $__index + 1;
      var __labelLast = $('#label-item' + lastIndex);
      var __lastTooltip = $('#tooltip-info-redis' + lastIndex);
      var relativeValuePos = 0;

      posX = itemSize*(lastIndex - 1)+ $__value*relativeCompWidth/maxValue;

      //background
      $('.range-bg-' + lastIndex).width(posLast - posX)
      setVisibleInfo(posLast , posX, __labelLast, __lastTooltip, itemSize, lastIndex);

      relativeValuePos = posX + (posLast - posX)/2;

      posX = relativeValuePos + itemSize/2 - __labelLast.width()/2;
      __labelLast.css({left:posX})

      posX = relativeValuePos - (__lastTooltip.width() + 2)/2;
      __lastTooltip.css({left:posX})

    }
    else {
      posX = (itemSize)*$__index  + $__value*relativeCompWidth/maxValue;
      var posBefore = (itemSize*($__index - 1))  + ($__beforeValue*relativeCompWidth/maxValue);
      var relativeValuePos = posBefore + (posX - posBefore)/2;

      //background
      $('.range-bg-' + $__index).width(posX - posBefore)

      //tooltip/label visible
      setVisibleInfo(posX , posBefore, __labelComp, __tooltip, itemSize, $__index);

      var posLabel = relativeValuePos + itemSize/2  - __labelComp.width()/2;

      //console.log('posLabel', posLabel)
      __labelComp.css({left: posLabel})

      var posTooltip = relativeValuePos - (__tooltip.width() + 2)/2;

      //console.log('posTooltip',relativeCompWidth)
      __tooltip.css({left: posTooltip})

    }
  })
}

//fix IE
function appendThumbListener(maxValue){
  var drag = d3.behavior.drag();
    //selection.call(drag);
  var value = 0;

    $('.range-thumb').each(function(){
      var index = parseInt($(this).attr('id').split('thumb')[1]);

      var  x = 0;
      var realX = 0;
      var range = $('#rangeMultiple' + index);
      var width = range.width() - itemSize;

      value += currentUserValues[index];
      realX =  value/maxValue*(width); //without margin
      x =  value/maxValue*(width) + (itemSize*index); //add margin

      //thumb initial
      var thumb =
          d3.selectAll('#thumb' + index)
      .data([ {"x":realX, "y":0} ])
      .style("left",  x + "px")
      .call(drag);

      //console.log('index', index)

      //after drag
      drag.on('drag', function(d,i){
        var index = parseInt(d3.select(this).attr('id').split('thumb')[1]);
        var range = $('#rangeMultiple' + index);
        var width = range.width() - itemSize;

        var graphicStepWidth = (0*width/maxValue);
        var valueMinWidth = graphicStepWidth*(index + 1);
        var valueMaxWidth = graphicStepWidth*((currentUserValues.length - 1) - index)

        //console.log(valueMinWidth)
        d.x += parseInt(d3.event.dx);

        //d.x = d.x ;
        //console.log(d.x)
        if(d.x < valueMinWidth) d.x = valueMinWidth;

        if(d.x > width - valueMaxWidth) d.x = (width - valueMaxWidth);

        var value = d.x/(width)*maxValue;

        range.val(value)
        range.change();
        d3
          .select(this)
          .style("left", (parseInt(d.x) + itemSize*index )+ "px")
      })
    })

}

//setup the bar
function setInitial(dataUserMapValues, maxValue, phonesNumber){
  var $container = $('.c-range-container');

  //convert to displayable to user view
  dataUserMapValues.forEach(function(item, index){
    //console.log('item', item)
    item = +convertSizeToNumberDisplayable(bytesToSize(item));
    dataUserMapValues[index] = item;
    originalValues.push(item);
  })

  currentUserValues = dataUserMapValues.slice();


  //convert max
  maxValue = +convertSizeToNumberDisplayable(bytesToSize(maxValue));
  console.log('maxValue', maxValue)

  //set max value
  $('.user-max-value').text(parseInt(maxValue/1000));

  appendBox(phonesNumber);

  for(i = 0; i < dataUserMapValues.length - 1; i++){
    var input = $('<input type="range" multiple></input>').clone();
    var thumb = $('<div class="range-thumb"></div>')
    thumb.attr('id','thumb' + i);

    $container.append(thumb);

    previusValue.push(0);

    input.attr("max" , maxValue)
    input.attr("id" , "rangeMultiple" + i);
    input.attr( "min" , 0);
    input.attr( "step" , graphicStep);
    input.val(dataUserMapValues[i] + beforeValue)
    input.css({'marginLeft': itemSize*i})
    $container.append(input);
    input.width($('#range').width() - itemSize*(dataUserMapValues.length - 2))


    //set value
    $('#value-box-' + i).text(getFormatedValue(currentUserValues[i]));
    if(i == currentUserValues.length - 2){
      var lastIndex = i + 1;
      $('#value-box-' + lastIndex).text(getFormatedValue(currentUserValues[lastIndex]));
    }


    beforeValue= parseFloat(input.val());

    //$('#range').width(input.width() + itemSize*(dataValues.length - 2));

    componentWidth = $('#range').width();

    //change thumb position
    var currentValue = currentUserValues[i];

    //var relWidth = componentWidth - (itemSize*(currentUserValues.length - 1));
    //var x =  currentValue/maxValue*(relWidth)

    //console.log(x)

    //changeThumb(x, i, currentValue)
  }

  appendLabelsAndTooltip(maxValue);
  updatePositionLabelsAndTooltips(maxValue);

  return maxValue;
}

function gigaInfo(output){
  output = output/1000

  if(parseInt(output) == (output)){
    return ( parseInt(output) + " GB");
  }

  return ((output).toFixed(1) + " GB");
}

function getFormatedValue(output){
  //console.log('isInteger' , Number.isInteger((output/1000).toFixed(1) ))
  //return (output > 999) ? (gigaInfo(output)) : (parseInt(output) + " MB")
  return gigaInfo(output);
}

function getSumWidthoutThisPosition(position){
  var sum = 0;

  currentUserValues.forEach(function(value,index){
    if(index != position) sum += parseInt(value);
  })


  return sum;
}

var getTotalSum = function(){
  var sum = 0;

  currentUserValues.forEach(function(value,index){
    sum += parseInt(value);
  })

  return sum;
}

var setLabelsAndValues = function(index, current, nextVal){
  var nextIndex = parseInt(index) + 1;
  currentUserValues[index] = current;
  currentUserValues[nextIndex] = nextVal;
  $('#value-box-' + index).text(getFormatedValue(current));
  $('#value-box-' + (nextIndex)).text(getFormatedValue(nextVal));
  $('#label-item' + index + ' .label-number').text(getFormatedValue(current));
  $('#label-item' + nextIndex + ' .label-number').text(getFormatedValue(nextVal));
  $('#tooltip-info-redis' + index + ' .content').text(getFormatedValue(current));
  $('#tooltip-info-redis' + nextIndex + ' .content').text(getFormatedValue(nextVal));
}

function updateValues(index, $__value){

  $__value = parseInt($__value);
  index = parseInt(index);
  var nextIndex = (index) + 1;
  var previusIndex = (index) - 1;
  var next = $('#rangeMultiple' + nextIndex);
  var nextVal = parseInt(next.val());
  var previus = $('#rangeMultiple' + previusIndex);
  var previusValue = parseInt(previus.val());


  if(index == 0){
    nextVal = nextVal - $__value;

    if(currentUserValues.length > 2){
      setLabelsAndValues(index, $__value, nextVal)
    }
    else {
      nextVal = maxValue - $__value;
      setLabelsAndValues(index, $__value, nextVal)
    }

  }
  //last
  else if(index + 2 == currentUserValues.length){
    var current = $__value - previusValue;
    nextVal = maxValue - $__value;
    setLabelsAndValues(index, current, nextVal)

    //console.log('last')

  } else {//middle
    var current = $__value - previusValue;
    nextVal = nextVal - $__value;
    setLabelsAndValues(index, current, nextVal)

    //console.log('middle')
  }

}

function collisionOnStartAndEnd($__index, $__value){
  var $__comp = $('#rangeMultiple' + $__index);

  var first = parseInt($__index) + 1;
  var last = (currentUserValues.length - 1) - parseInt($__index);


  //start
  if($__value < 0*first){
    $__comp.val(0*first);
    $__comp.change();
  }

  //end
  if($__value > maxValue - 0*last){
    $__comp.val(maxValue - 0*last);
    $__comp.change();
  }
}

function changeThumb(x , index,value){
  var dx = x + itemSize*index;
  $('#thumb' + index).css({left:dx})
  d3
    .select('#thumb' + index)
    .datum({"x":x})
}

$(window).ready(function(){


  initBarRedistribuicaoAgendada(
    {
      data: [4000, 12000, 6000, 10000, 8000],
      phones: phones,
      max: 40000
    }
  );

  initModal(
    {
      data: [15900, 100, 8000, 1000, 15000],
      phones: phones,
      max: 40000
    }
  )

  //setup initial sliders
  maxValue = setInitial(dataUser, dataMax, phones);

  //append listeners to thumbs
  appendThumbListener(maxValue);

  $('#restaurar__saldo').click(function(event){
    event.preventDefault();
    event.stopPropagation();

    var current = 0;

    originalValues.forEach(function(value, index){
      var comp =   $('#rangeMultiple' + index);
      current += value;

      comp.val(current);
      comp.change();
    })
  })

  //changeInputBGColor();
  $('#write-button').click(function(event){
    event.preventDefault();
    event.stopPropagation();

    var max = maxValue;
    var current = 0;

    //var currentUserValues
    var value = parseInt(maxValue/currentUserValues.length);
    value = (value/1000).toFixed(1)*1000;
    //console.log(value)

    for(i = 0; i < currentUserValues.length ; i++){
      var comp =   $('#rangeMultiple' + i);

      currentUserValues[i] = value;
      max -= value;

      if(i == currentUserValues.length - 1){
        if(max > 0 || max < 0) currentUserValues[i] += max;
      }

      current += currentUserValues[i];
      comp.val(current);
      comp.change();
    }

  })



  $('.btn.btn-default.btn-block-transferir-saldo.btn-transferir-saldo')
    .click(function(){
      //alert('apply' + currentUserValues.toString())

      applyChangesToServer(currentUserValues);
  })

  $('input[type=range][multiple]').each(function(){
    //console.log($(this).attr('id'))
    var id = $(this).attr('id');

    //console.log(id)

    $(document).on('input change', "#" + id , function() {

        var $__value = parseInt($(this).val());
        var $__index = $(this).attr('id').split('rangeMultiple')[1];
        var $__direction = getDirection($__index , $__value);
        //console.log($__direction)

        //test collision and change position
        changePositions($__direction , $__index, $(this));

        //set previus
        previusValue[$__index] = $__value;

        updateValues($__index, $__value);
        updatePositionLabelsAndTooltips(maxValue);

        collisionOnStartAndEnd($__index, $__value)

        var relWidth = componentWidth - (itemSize*(currentUserValues.length - 1));
        var x =  $__value/maxValue*(relWidth)


        changeThumb(x, $__index, $__value)

        //changeInputBGColor()

    })
  })
})

function applyChangesToServer(currentValues){
  var values = currentValues.slice(0); //clone data
  //o valor mínimo que um usuário pode ter é 100KB
  var __100KB = Math.pow(2,10) * 100;
  var __100MB = Math.pow(2,20) * 100;
  var __GB = Math.pow(2, 30);

  //convert to bytes
  values.forEach(function(value, index){
    var intValue = parseInt(value/1000);
    var fracional = parseInt(value - intValue*1000);
    value = intValue * Math.pow(2,30) + fracional*Math.pow(2,20);
    values[index] = value;
  })

  if(values){
    values.forEach(function(item, index){

      var coldRemove = true;
      var currentIndex = 0;
      var nextValue = 0;

      //try remove value from other slider
      if(item == 0 || item === '0'){

        //try from left
        while(coldRemove && currentIndex >= 0){

          nextValue = values[currentIndex];

          if(nextValue > __100KB && nextValue){
            values[index] = __100KB;//add 100KB
            values[currentIndex] = values[currentIndex] - __100KB;//remove 100KB
            coldRemove = false;
            break; //exit while
          }

          //if(!nextValue) break;

          currentIndex--;
        }

        currentIndex = index + 1;

        //try from right
        while(coldRemove
          && currentIndex < values.length){

          nextValue = values[currentIndex];

          if(nextValue > __100KB && nextValue){
            values[index] = __100KB;//add 100KB
            values[currentIndex] = values[currentIndex] - __100KB;//remove 100KB
            coldRemove = false;
            break; //exit while
          }

          //if(!nextValue) break;

          currentIndex++;
        }
      }

    })

    sendDataToServer(values)
  }
}

function sendDataToServer(values){
  //your implementation here

  //remove code below
  alert(values.map(function(item){
    return 'User: ' + bytesToSize(item) + ' , Server: ' + item + 'bytes\n';
  }))
}
