/*release version*/
$(document).ready(function(){

  //PRJ4740 - adequacao tamanho selectbox - START
  $('div.submotivo').click(function(){
    var motivo = $('div.motivo .sbSelector').html();
    if (motivo !== 'Sugestão'){
      $('div.submotivo .sbOptions').css('min-height','280px');
    }
  });
  //PRJ4740 - adequacao tamanho selecbox - END

// Defeito 2082 - PRJ 4740 - Bloquear botao de submissao para evitar multiplas requisicoes ao servico web.
  var formFaleConosco = $( "#solicitarFaleConosco" );
  formFaleConosco.submit(function( event ) {
	  if (formFaleConosco.valid()){
      var numWithoutMask = $('#outroNumero').val().replace('(','').replace(')','').replace('-','');
      $('#outroNumero').val(numWithoutMask);
		  $('#solicitarFaleConosco button').prop('disabled', true);
		  $(document.body).css({ 'cursor': 'wait' });
	  }
	});

//15018 - start
  if ($('form#alteracaoDadosAcesso input#tel').length > 0){
    $('form#alteracaoDadosAcesso input#tel').keyup(function(){
      var mobilePhone = $('form#alteracaoDadosAcesso input#tel').val();
      if (mobilePhone.length >= 4){
        var mobileDigit = mobilePhone.charAt(4);
        if (mobileDigit < 5){
          if ($('div.input.tel small.error').length <= 0){
              $('div.input.tel').append("<small id='senha-error' class='error' style='display: inline;'>Telefone M&oacute;vel inv&aacute;lido</small>");
          }
        } else {
            $('div.input.tel small.error').remove();
        }
      }
    });
  }

  if ($('form#alteracaoDadosAcesso input#senhaConfirm').length > 0){
      $('form#alteracaoDadosAcesso input#senhaConfirm').blur(function(){
        var passwd = $('form#alteracaoDadosAcesso input#senha').val();
        var confPasswd = $('form#alteracaoDadosAcesso input#senhaConfirm').val();
        if (confPasswd.length > 5 && passwd !== confPasswd){
            if ($('div.input.senhaConfirm small.error').length <= 0){
                $('div.input.senhaConfirm').append("<small id='senha-error' class='error' style='display: inline;'>Confirma&ccedil;&atilde;o de Senha inv&aacute;lida</small>");
              }
        } else {
            $('div.input.senhaConfirm small.error').remove();
        }
      });
    }
    //PRJ 15018 - end

  //defeito 34 prj 1496 - remover filtro tipo ligacao das caixas de Servico - start
  $('.detalhamentoConsumo-filtros-Ligacao b').each(function(index){
    var divCaixa = $(this).parent().parent().parent().parent().parent();
    var tituloCaixa = divCaixa.find('h4:first-child').html();
    if (tituloCaixa.toLowerCase().indexOf('serviços') > -1){
      $(this).parent().parent().css('display', 'none');
    }
  });
  //defeito 34 prj 1496 - end

  //PKE 18315 - remove botao quando cliente nao tiver ponto extra de TV (solucao temporaria, deve ser refatoardo o JSP)
  if ($.trim($('.tooltip-tv-pacotes > .arrow-box.tooltip-inner.simple-text > ul').html()) == "" && $('.tooltip-tv-pacotes > .arrow-box.tooltip-inner.simple-text > p').length == 0){
  $('.btn-plano-descricao').css("display", "none");
  }

	carregaTooltipHover('.box-header [data-toggle="tooltip"]', 18, 20, 'arrow-box tooltip-inner simple-text');

  //Tooltip for home produto
  carregaTooltipHover('#tooltip-telefones-pacotes', 15, 20, 'arrow-box tooltip-inner simple-text', 135);
  carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text', 135);
  carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list', 135);
  carregaTooltipHover('#bt-segunda-via-indisponivel', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list', 135);

  if($('#sso-home').length == 0) {
    menuLateralInit();
  }

  //Checks if plan name has more than 50 chars and it's not inside a combo box
  var nomesPlanos = $(".box-header h3");

  nomesPlanos.each( function () {
    var plano = $(this).html();
    //for (var i = plano.length - 1; i >= 0; i--) {
    if(plano.indexOf('<') > 0) {
      var tituloPlano = plano.slice( 0 , plano.indexOf('<') - 1 ).trim();
      var terminalPlano = plano.slice( plano.indexOf('<') , plano.length );
      if(tituloPlano.length > 40 && tituloPlano.indexOf('...') == -1){
        // Char limit for selected value
        var tituloPlanoCutted;
        tituloPlanoCutted = tituloPlano.substring(0,39) + '...';
        tituloPlano = tituloPlanoCutted;
        $(this).html(tituloPlanoCutted + terminalPlano);
      }
    }
  });


  //Detalhes e Servicos - Start
  var listaBotoesDetalhesServicos = $('a[id^="detalhe-"]');
  if (listaBotoesDetalhesServicos.length > 0) {
    listaBotoesDetalhesServicos.hide();

    $('a[id="detalhe-combo-0"]').show();
    $('a[id="detalhe-movel-0"]').show();
    $('a[id="detalhe-fixo-0"]').show();
    $('a[id="detalhe-internet-0"]').show();
    $('a[id="detalhe-tv-0"]').show();
  }
  //Detalhes e Servicos - End

  // Selectbox for OCT Legacy
  $("#box-combo .resumo-conta .oct-legado").hide();

if ($("#box-combo #select-aba-resumo").length > 0){
  $("#box-combo #select-aba-resumo").selectbox({
      onChange : function (value, inst) {

      var selectboxId = $(this).attr("id");
      var containerSelect = $(this).parent().parent();
      var optSelected = containerSelect.find('a[id^="sbSelector"]').html();

      $('#' + selectboxId + ' option').each(function (){

        if($(this).val() == optSelected) {
          $(this).prop('selected', true);
        }
      });

      if (value === "Combo") {
        $("#box-combo .resumo-conta .oct-legado").hide();
        $("#box-combo .resumo-conta .combo").show();
      } else {
        $("#box-combo .resumo-conta .combo").hide();
        $("#box-combo .resumo-conta .oct-legado").show();
      }
    }
  });
}
  // Collapse Saldo Dados
  if ($(".control-collapse span").hasClass("detalhes")) {

    $(".saldo-dados .barraRealConsumo .consumo3g p").show();
    $(".saldo-dados .item-plano .boxRealConnsumo").hide();
    $(".saldo-dados .extraInfo").hide();
    $(".saldo-dados .saldoN").hide();
    $(".saldo-dados .extraInfoComplementar").hide();
  }

  $("#collapse-saldo-dados").on( "click", function(){

    if ($(this).parent().find("span").hasClass("ocultar")) {

      $(".saldo-dados .barraRealConsumo .consumo3g p").show();
      $(".saldo-dados .item-plano .boxRealConnsumo").hide();
      $(".saldo-dados .extraInfo").hide();
      $(".saldo-dados .saldoN").hide();
      $(".saldo-dados .extraInfoComplementar").hide();
      $(this).text("+ Detalhes");
      $(this).parent().find("span").removeClass("ocultar");
      $(this).parent().find("span").addClass("detalhes");

    } else {

      $(".saldo-dados .barraRealConsumo .consumo3g p").show();
      $(".saldo-dados .item-plano .boxRealConnsumo").show();
      $(".saldo-dados .extraInfo").show();
      $(".saldo-dados .saldoN").show();
      $(".saldo-dados .extraInfoComplementar").show();
	  $(".consumo3g .warnExceed.extraInfo").css('display', 'block');
	  if(($('#consumoExcedenteOne').length > 0) || ($('#consumoExcedente').length > 0)) {
		$(".consumo3g .warnExceed.extraInfo").css('display', 'block');
		$(".consumo3g.consumoComplementar #warnExceedOne").hide();
		if ($('#consumoExcedente').length <= 0) {
			$('.boxRealConnsumo').css('padding-top', '0px');
		}
	  }
      $(this).text("- Ocultar");
      $(this).parent().find("span").removeClass("detalhes");
      $(this).parent().find("span").addClass("ocultar");
    }
  });

  // Saldo Parcial tabs
  var aba_1 = $("#graficos-tab");
  var aba_2 = $("#tabelas-tab");

  aba_2.hide();
  $("#ico-grafico").addClass('active');
  $("#ico-grafico").on('click', function(event) {

    aba_2.hide();
    aba_1.show();
    $(this).parent().parent().find('.active').removeClass('active');
    $(this).addClass('active');
    $(".saldo-parcial .control-collapse").show();

    event.preventDefault();

  });

  $("#ico-lista").on('click', function(event) {

    aba_1.hide();
    aba_2.show();
    $(this).parent().parent().find('.active').removeClass('active');
    $(this).addClass('active');
    $(".saldo-parcial .control-collapse").hide();

    event.preventDefault();
  });

  // Collapse Saldo Parcial
  $("#collapse-saldo-parcial").on('click', function(){
    if ($(this).parent().find("span").hasClass("ocultar")) {

      //$(".saldo-parcial .consumoParcial .barraRealConsumo strong:first-child").hide();
      $(".saldo-parcial .extraInfo").hide();
      $(this).text("+ Detalhes");
      $(this).parent().find("span").removeClass("ocultar");
      $(this).parent().find("span").addClass("detalhes");

    } else {
      $(".saldo-parcial .extraInfo").show();
      $(this).text("- Ocultar");
      $(this).parent().find("span").removeClass("detalhes");
      $(this).parent().find("span").addClass("ocultar");
    }
  });

  //PRJ00014297 - Debito Automatico - c.s. - start
  if ($(".confirma-dacc").find('.sidebysidepar').length === 0) {
    if($('.boxsidebysideinner').length > 1) {
      $('.um-produto.vertical-center').css('min-height','35vh');
    }

    $('.confirma-dacc .boxsidebysidebottom').removeClass('.boxsidebysidebottom');
  }

  if($('.um-produto.col-md-6.vertical-center').length > 0){
    $('.um-produto.col-md-6.vertical-center').height($('.containerFatura.col-md-6').height())
  }

  //Somente para tirar evidencias
  $('.account-moreActions-internas').hide();
  //PRJ00014297 - Debito Automatico -  c.s. - end

  //PRJ0001496 - E-billing/Detalhamento de Consumo - c.s. - start
  var idColunaEscondida = 0;
  $('.fatura-detalhamentoConsumo .detalhamentoConsumo-tipoServico .detalhamento-dados').each(function() {
    var tituloColunasServico = $(this).find('th');
    var dadosServico = $(this).find('td');

    tituloColunasServico.each(function() {
      if ($(this).html() === 'null' || $(this).html() === " ") {
        $(this).hide();

        idColunaEscondida = $(this).index();
        var meusDados = $(this).parent().find('td');
        dadosServico.each(function() {
          if ($(this).index() === idColunaEscondida) {
            $(this).hide();
          }
        });

      } else {
        $(this).show();
      }
    });
  });

  $('select[id^="filtro-tipoLigacao"]').each(function(){
    $(this).on('change', function() {
      var boxId = $(this).attr("id").split("_")[1];
      var selectedValue = $(this).find(":selected").text();

      $("div[id^=tituloTemplate").each(function(){
        if ($(this).attr("id").split("_")[2] === boxId){
          $(this).show();
        }
      });

      if (selectedValue != "Todos"){

        var divId = "div[id='tituloTemplate_" + selectedValue + "_" + boxId + "']";

        $("div[id^=tituloTemplate").each(function(){
          if ($(this).attr("id").split("_")[1] != selectedValue && $(this).attr("id").split("_")[2] === boxId){
            $(this).hide();
          }
        });
      }
    });
  });
  //PRJ0001496 - E-billing/Detalhamento de Consumo -  c.s. - end

      // Adiciona os links de cadastro de Conta Online e Debito automatico
      if($(".dacc-conta-online .links").length > 0 && typeof csp !== 'undefined' && typeof dacc !== 'undefined'){
        var tagContaOnline = $("<a class='contacsp'>").text(csp);

        if(csp == "Cadastrar" ){
          tagContaOnline.addClass("cadastrar").attr("href","/portal/site/MinhaOi/AdesaoCSP");
        }else if(csp == "Cadastrado" ){
          tagContaOnline.addClass("cadastrado");
        }

        var tagDebtoAutomatico =  $("<a class='debito-automatico'>").text(dacc);

        if(dacc == "Cadastrar" ){
          tagDebtoAutomatico.addClass("cadastrar").attr("href","/portal/site/MinhaOi/debito-automatico");
        }else if(dacc == "Cadastrado" ){
          tagDebtoAutomatico.addClass("cadastrado");
        }

        $(".dacc-conta-online .links").append(tagContaOnline).append(tagDebtoAutomatico);
      }

      //Change do select-box plugin
      atrelaChangeAoSelectboxMesReferencia();

      /* PRJ15018 - Start */
      $('#home-produto .dados-acesso').removeClass('ipt');
      /* PRJ15018 - End */

      /* Carrega tela de Meus Dados de Acesso */
      if (($('form#alteracaoDadosAcesso').length > 0)){
        var ddd = $("#ddd").val();
        var phoneNumber = $("#telMovel").val();
        var maskedPhone;
        if (phoneNumber.length == 9){
          maskedPhone = phoneNumber.substring(0,5) + "-" + phoneNumber.substring(5);
        } else {
          maskedPhone = phoneNumber.substring(0,4) + "-" + phoneNumber.substring(4);
        }
        $("#tel").val("("+ddd+")"+maskedPhone);

        if($("#recSms").val() == "true"){
          $("#receberSms").prop( "checked", true );
        }

        if($("#recNews").val() == "true"){
          $("#receberNewsletter").prop( "checked", true );
        }
      }

      $("#dadosTitularForm").submit(function() {
        var DDD = $("#numero-oi").val().substr(1,2);
        var Numero = $("#numero-oi").val().substr(5);
        $("#codigoArea").val(DDD);
        $("#terminalNumero").val(Numero);
      });
}); // Fim document ready
// --------------------------------------------

// Method that performs layout changes in sidebar Main menu
function sidebarMenu ( itemClicked, idItemClicked ) {
  var anteriorAtivo = $("#nav-menu-principal ul").find("li.active");
  var ativoAtual = itemClicked.parent();

  if (itemClicked.attr("id") == idItemClicked) {

    event.preventDefault();

  } else {
    if (ativoAtual.children().hasClass("sub-navbar")) {
      if(itemClicked.find("span.glyphicon").hasClass("glyphicon-menu-down")) {
        itemClicked.find("span.glyphicon").removeClass("glyphicon-menu-down");
        itemClicked.find("span.glyphicon").addClass("glyphicon-menu-up");

      } else {
        itemClicked.find("span.glyphicon").removeClass("glyphicon-menu-up");
        itemClicked.find("span.glyphicon").addClass("glyphicon-menu-down");
      }

            ativoAtual.children(".sub-navbar").toggle();
        }
    }

}

// List of functions to detect browser
function isIE () {
  var isIE = /* @cc_on!@ */false || !!document.documentMode; // At least IE6
  return isIE;
}

function isFirefox () {
  var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
  return isFirefox;
}

function isChrome () {
  var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
  return isChrome;
}

function isSafari () {
  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
  // At least Safari 3+: "[object HTMLElementConstructor]"
  return isSafari;
}

function isOpera () {
  var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
  return isOpera;
}

function menuLateralInit () {
  var itemAtivo = '';
  // itemAtivo = $("#nav-menu-principal .nav-sidebar").find(".active");

  if (window.location.toString().indexOf("home_produto_combo") != -1 || $('#home-produto #box-combo').length > 0) {
    itemAtivo = $('#combo-bt').parent().addClass('active');
  } else if (window.location.toString().indexOf("home_produto_mvl") != -1 || $('#home-produto #box-movel').length > 0) {
    itemAtivo = $('#movel-bt').parent().addClass('active');
  } else if (window.location.toString().indexOf("home_produto_fxo") != -1 || $('#home-produto #box-fixo').length > 0) {
    itemAtivo = $('#fixo-bt').parent().addClass('active');
  } else if (window.location.toString().indexOf("home_produto_int") != -1 || $('#home-produto #box-internet').length > 0) {
    itemAtivo = $('#internet-bt').parent().addClass('active');
  } else if (window.location.toString().indexOf("home_produto_ptv") != -1 || $('#home-produto #box-tv').length > 0) {
    itemAtivo = $('#tv-bt').parent().addClass('active');
  } else {
    itemAtivo = $('#inicio-bt').parent().addClass('active');
  }

  var idItemAtivo = '';
  // Fecha todos os possiveis submenus
  $("ul[id$='-submenu']").toggle(false);

  if(itemAtivo.hasClass("submenu")){
    idItemAtivo = $(".submenu.active a:first-child").attr("id");
    $(".submenu.active .sub-navbar").show();
    $(".submenu.active a[id$='-bt'] span.glyphicon").addClass("hidden");

    $(".submenu.active .sub-navbar li").each(function(){
      var planoAtivo = $('h2 a').text();
      if ($(this).find('.item-plano:contains(' + planoAtivo + ')').text()) {
        if (idItemAtivo == 'combo-bt') {
          var terminalTitularAtivo = $('#idProdutoHome').val();
          if ($(this).find('input[id^=produto-]').val() == terminalTitularAtivo) {
            $(this).addClass('active');
          }
        } else if (idItemAtivo == 'movel-bt') {
          var terminalTitularAtivo = $('#idProdutoHome').val();
          if ($(this).find('input[id^=produto-]').val() == terminalTitularAtivo) {
            $(this).addClass('active');
          }
        } else if (idItemAtivo == 'fixo-bt') {
          var terminalTitularAtivo = $('#idProdutoHome').val();
          if ($(this).find('input[id^=produto-]').val() == terminalTitularAtivo) {
            $(this).addClass('active');
          }
        } else if (idItemAtivo == 'internet-bt') {
          var terminalTitularAtivo = $('#idProdutoHome').val();
          if ($(this).find('input[id^=produto-]').val() == terminalTitularAtivo) {
            $(this).addClass('active');
          }
        } else if (idItemAtivo == 'tv-bt') {
          var terminalTitularAtivo = $('#idProdutoHome').val();
          if ($(this).find('input[id^=produto-]').val() == terminalTitularAtivo) {
            $(this).addClass('active');
          }
        } else {
          $(this).addClass('active');
        }
      }
    });
  } else {
    $(".submenu a[id$='-bt'] span.glyphicon").removeClass("hidden");
    idItemAtivo = itemAtivo.attr('id');
  }

  // Links for sidebar Main menu
    $("#inicio-bt").click(function() {
        sidebarMenu($(this), idItemAtivo);
    });
    $("#combo-bt").click(function() {
        sidebarMenu($(this), idItemAtivo);
    });
    $("#movel-bt").click(function() {
        sidebarMenu($(this), idItemAtivo);
    });
    $("#fixo-bt").click(function() {
        sidebarMenu($(this), idItemAtivo);
    });
    $("#internet-bt").click(function() {
        sidebarMenu($(this), idItemAtivo);
    });
    $("#tv-bt").click(function() {
        sidebarMenu($(this), idItemAtivo);
 	});
}


function carregaTooltipHover (seletor, offsetTop, offsetTopInner, classesTemplate) {

  //Removido para fazer o tooltip funcionar em um retorno de uma requisicao ajax
  if (isIE()) {

        $("body").tooltip({
            selector: '[data-toggle=tooltip]',
            template: '<div class="tooltip extra-height"><div class="' + classesTemplate + '"></div></div>',
            html: true,
            delay: {
                "show": 0,
                "hide": 3000
            }
        });
    } else {
        $(seletor).hover(function() {
            $(this).tooltip({
                container: $(this).parent(),
                template: '<div class="tooltip extra-height"><div class="' + classesTemplate + '"></div></div>',
                placement: 'bottom',
                html: true,
                trigger: 'manual'
            }).tooltip('show');

            var position = $(this).parent().find('div[id^="tooltip"]').position();
            $(this).parent().find('div[id^="tooltip"]').css('top', position.top - offsetTop);
            $(this).parent().find('.arrow-box.tooltip-inner').css('top', position.top - offsetTopInner);

        }, function(e) {
            $('div[id^="tooltip"]').mouseleave(function() {
                $(this).fadeOut('slow');
            });
        }
        );
    }
}

function carregaTooltipHover(seletor, offsetTop, offsetTopInner, classesTemplate, offsetCustomHeight) {
    $(seletor).hover(function() {
        var id = $(this).attr('id');
        $(this).tooltip({
            container: $(this).parent(),
            template: '<div class="tooltip extra-height ' + id + '"><div class="' + classesTemplate + '"></div></div>',
            placement: 'bottom',
            html: true,
            trigger: 'manual'
        }).tooltip('show');
        var id = $(this).attr('id');
        $('.' + id).css('opacity', '1');

        var position = $(this).parent().find('div[id^="tooltip"]').position();
        $(this).parent().find('div[id^="tooltip"]').css('top', position.top - offsetTop);
        $(this).parent().find('.arrow-box.tooltip-inner').css('top', position.top - offsetTopInner);

        // CSS usado para debug
        $('.tooltip.extra-height').css({
            'height': offsetCustomHeight
        });
      },
      function(e) {
        $(seletor).mouseleave(
        		function() {
        			var id=$(this).attr('id');
        			$('.'+id).fadeOut('fast');
        			$('.'+id).hover(
        					function(){
        						$('.'+id).stop();
        					},
        					function(){
        						$('.'+id).mouseleave(
        								function() {
        									$('.'+id).fadeOut('fast');
        								}
        						);

        					});
        		}
        );
  });
}

// CADASTRAR USUARIO - CARREGA TOOLTIP PERSONALIZADO - START
function carregaTooltipHoverCadastrarUsuario(seletor, offsetTop, offsetTopInner, classesTemplate) {

  $(seletor).hover(
    function() {
      $(this).tooltip({
        container: $(this).parent(),
        template: '<div class="tooltip extra-height"><div class="' + classesTemplate + '"></div></div>',
        placement: 'right',
        html: true,
        trigger: 'auto',
        delay: { "show": 0, "hide": 0}
      }).tooltip('show');

      var position = $(this).parent().find('div[id^="tooltip"]').position();
      $(this).parent().find('div[id^="tooltip"]').css('top', position.top - offsetTop);
      $(this).parent().find('.arrow-box.tooltip-inner').css('top', position.top - offsetTopInner);
    },
    function(e) {
      $('div[id^="tooltip"]').mouseleave(function() {
        $(this).fadeOut('fast');
      });
    }
    );
}

// Funcao que carrega a imagem mostrando onde esta o numero do cliente ao clicar na opcao na tela de dados gerais
function carregaImagemTooltipCliqueAqui() {
 $("#img-numero-movel").show();
}


// CADASTRAR-USUARIO - CARREGA TOOLTIP PERSONALIZADO - END


function addTitleOnItem(classHTMLName,className, labelName) {

 var optionsWithChangeMethod =  {
   onChange: function(classHTMLName){
    addTitleChangedItemCadastroUsuario($(className),labelName);
  },
  onComplete: function(classHTMLName) {
   addTitleSelectedItemCadastroUsuario($(className),labelName);
}
};

var optionsWithoutChangeMethod =  {
  onComplete: function(classHTMLName) {
   addTitleSelectedItemCadastroUsuario($(className),labelName);
}
};

// Adiciona Mascaras nos Campos
if (className == '.iptCPF'){
  $(className).mask('999.999.999-99',optionsWithChangeMethod);
}else if (className == '.iptNascimento'){
  $(className).mask('99/99/9999',optionsWithoutChangeMethod);
}else if (className == '.iptCEP'){
  $(className).mask('99999-999',optionsWithoutChangeMethod);
}else if (className == '.iptNumeroOi'){
  $(className).mask('(00)00000-0000',optionsWithoutChangeMethod);
}else if (className == '.iptNumeroContrato'){
  $(className).mask('0000000000',optionsWithoutChangeMethod);
}


}

// Para validacao de senha atraves do tooltip

$(function(){
  $('.tooltipValidacao').hide();

  $('#senha').focusin(function(){
    $('.validacao.tooltipValidacao').parent().addClass('labelSenha');
    pos = $(this).position();
    $('.validacao.tooltipValidacao').show();
  }).focusout(function(){
    $('.validacao.tooltipValidacao').hide();
  });

  var characterLimit = 10;
    // Check if the password contains at least 1 letter and 1 digit
    var pattern = /^(?=.*[a-z])(?=.*\d).+$/i;

    $('#senha').bind('keyup', function(e){

      var passwordStr = $(this).val();
      var charactersUsed = $(this).val().length;

      if(charactersUsed > characterLimit){
        charactersUsed = characterLimit;
        $(this).scrollTop($(this)[0].scrollHeight);
      }

      var charactersRemaining = characterLimit - charactersUsed;

      if(charactersRemaining < 5){
        $('.validacao.tooltipValidacao li:first-child').addClass('isValid');
        $('.validacao.tooltipValidacao li:first-child p').addClass('isValid');

      }else{
        $('.validacao.tooltipValidacao li:first-child').removeClass('isValid');
        $('.validacao.tooltipValidacao li:first-child p').removeClass('isValid');
      }

      if(passwordStr.match(pattern)){
        $('.validacao.tooltipValidacao li:last-child').addClass('isValid');
        $('.validacao.tooltipValidacao li:last-child p').addClass('isValid');
      }else{
        $('.validacao.tooltipValidacao li:last-child').removeClass('isValid');
        $('.validacao.tooltipValidacao li:last-child p').removeClass('isValid');
      }

    });

    $('#senhaConfirmacao').bind('keyup', function(e){

      var passwordStr = $(this).val();
      var charactersUsed = $(this).val().length;

      if(charactersUsed > characterLimit){
        charactersUsed = characterLimit;
        $(this).scrollTop($(this)[0].scrollHeight);
      }

      var charactersRemaining = characterLimit - charactersUsed;

      if(charactersRemaining < 5){
        $('.confirmaValidacao.tooltipValidacao li:first-child').addClass('isValid');
        $('.confirmaValidacao.tooltipValidacao li:first-child p').addClass('isValid');
      }else{
        $('.confirmaValidacao.tooltipValidacao li:first-child').removeClass('isValid');
        $('.confirmaValidacao.tooltipValidacao li:first-child p').removeClass('isValid');
      }

      if(passwordStr.match(pattern)){
        $('.confirmaValidacao.tooltipValidacao li:nth-child(2)').addClass('isValid');
        $('.confirmaValidacao.tooltipValidacao li:nth-child(2) p').addClass('isValid');
      }else{
        $('.confirmaValidacao.tooltipValidacao li:nth-child(2)').removeClass('isValid');
        $('.confirmaValidacao.tooltipValidacao li:nth-child(2) p').removeClass('isValid');
      }

      if ($('#senha').attr('value') != '' && ($(this).attr('value') == $('#senha').attr('value'))) {
        $('.confirmaValidacao.tooltipValidacao li:last-child').addClass('isValid');
        $('.confirmaValidacao.tooltipValidacao li:last-child p').addClass('isValid');
      }else{
        $('.confirmaValidacao.tooltipValidacao li:last-child').removeClass('isValid');
        $('.confirmaValidacao.tooltipValidacao li:last-child p').removeClass('isValid');
      }

    });
  });

/* START 80681 */
  $(".divContratoTvIpt").hide();

  $("#linkCliqueAquiContratoTv").click(function(event){
    // esconder o input do terminal
    $(".divIptNumeroOi").hide();
    // limpar o campo de contrato TV
    $(".iptContrato").val("");
    // mostrar o input Contrato de TV
    $(".divContratoTvIpt").show();
    // esconder o link "clique aqui"
    $(".linkOiTvAlone").hide();

  });
  /* END 80681 */

function voltarMinhaOi(){
  window.location = "https://minha.oi.com.br"
}

document.addEventListener("click", function() {
    $('#oi-int-csp').remove();
});

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/* Branch 13440 - Confirmacao Adesao Conta Sem Papel - START */
$('body:has(.mlRmkt)').css("background", "none");
/* Branch 13440 - Confirmacao Adesao Conta Sem Papel - END */

/*PRJ 15018 - start*/
function validate(){
  if ($('.dados-acesso input#email').val().length > 0 && $('.dados-acesso input#senha').val().length > 0 && $('.dados-acesso input#tel').val().length > 0) {
    $("form#alteracaoDadosAcesso input[type=submit]").prop("disabled", false);
    $(".dados-acesso input[type=checkbox]").prop("disabled", false);
    $('.dados-acesso .aviso-oi label').removeClass("disabled");
  } else {
    $("form#alteracaoDadosAcesso input[type=submit]").prop("disabled", true);
    $(".dados-acesso input[type=checkbox]").prop("disabled", true);
    $('.dados-acesso .aviso-oi label').addClass("disabled");
  }
}

function validateSenha(){
  if ($('.dados-acesso input#senha').val().length > 0) {

    $('.dados-acesso .senhaConfirm label').removeClass("disabled");
    $('.dados-acesso input#senhaConfirm').prop("disabled", false)
  }
  else {
    $('.dados-acesso .senhaConfirm label').addClass("disabled");
    $('.dados-acesso input#senhaConfirm').prop("disabled", true)
  }
}

function mascaraTelefone( campo ) {

        function trata( valor,  isOnBlur ) {
					valor = valor.replace(/\D/g,"");
					valor = valor.replace(/^(\d{2})(\d)/g,"($1)$2");

					if( isOnBlur ) {
						valor = valor.replace(/(\d)(\d{4})$/,"$1-$2");
					} else {
						valor = valor.replace(/(\d)(\d{3})$/,"$1-$2");
					}
					return valor;
				}

				campo.onkeypress = function (evt) {
					 if(this.value.length == 14){
					 	return;
					 }
					var code = (window.event)? window.event.keyCode : evt.which;
					var valor = this.value

					if(code > 57 || (code < 48 && code != 8 ))  {
						return false;
					} else {
						this.value = trata(valor, false);
					}
				}

				campo.onblur = function() {
					var valor = this.value;
					if( valor.length < 13 ) {
						this.value = ""
					}else {
						this.value = trata( this.value, true );
					}
				}

				campo.maxLength = 14;
}
/*PRJ 15018 - end*/
