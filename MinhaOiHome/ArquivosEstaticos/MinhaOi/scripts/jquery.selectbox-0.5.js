/*
 * jQuery selectbox plugin
 *
 * Copyright (c) 2007 Sadri Sahraoui (brainfault.com)
 * Licensed under the GPL license and MIT:
 *   http://www.opensource.org/licenses/GPL-license.php
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * The code is inspired from Autocomplete plugin (http://www.dyve.net/jquery/?autocomplete)
 *
 * Revision: $Id$
 * Version: 0.5.1
 * 
 * Changelog :
 *  Version 0.5 
 *  - separate css style for current selected element and hover element which solve the highlight issue 
 *  Version 0.4
 *  - Fix width when the select is in a hidden div   @Pawel Maziarz
 *  - Add a unique id for generated li to avoid conflict with other selects and empty values @Pawel Maziarz
 versao Fabrica - leo.carvalho
 */
jQuery.fn.extend({
	selectbox: function(options) {
		return this.each(function() {
			new jQuery.SelectBox(this, options);
		});
	}
});


/* pawel maziarz: work around for ie logging */
if (!window.console) {
	var console = {
		log: function(msg) { 
	 }
	}
}


jQuery.SelectBox = function(selectobj, options) {
	
	var opt = options || {};
	opt.inputClass = opt.inputClass || "selectbox";
	opt.containerClass = opt.containerClass || "selectbox-wrapper";
	opt.hoverClass = opt.hoverClass || "current";
	opt.currentClass = opt.selectedClass || "selected"
	opt.debug = opt.debug || false;
	
	var elm_id = selectobj.id;
	var active = -1;
	var inFocus = false;
	var hasfocus = 0;
	//jquery object for select element
	var $select = $(selectobj);
	// jquery container object
	var $container = setupContainer(opt);
	//jquery input object 
	var $input = setupInput(opt);
	// hide select and append newly created elements
	$select.hide().before($input).before($container);
	$select.parents('label').click(function () {
	    return false;
	})
	
	init();
	
	$input
	.click(function(){
	   if (!inFocus) {
	       inFocus = true;
	       reloadSelectboxPosition()
	       active=0;
	       var lis=$('li', $container);
	       for(var l=0;l<lis.length;l++){
	         if($(lis[l]).hasClass(opt.currentClass)){
	           active=l;
	           break;
	         }
	       }
	       $container.show();
	       scrollToActive(active);
	   }else{
	       inFocus = false;
	       $container.hide();
	   }
	})
	.focus(function(){
	    if (!inFocus){
	        reloadSelectboxPosition()
	        $container.toggle();
            scrollToActive(active);
	    }
	})
	.keydown(function(event) {	   
		switch(event.keyCode) {
			case 38: // up
				event.preventDefault();
				moveSelect(-1);
				break;
			case 40: // down
				event.preventDefault();
				moveSelect(1);
				break;
			case 9:  // tab 
			    hideMe();
			    break;
			case 13: // return
				event.preventDefault(); // seems not working in mac !
				$('li.'+opt.hoverClass).trigger('click');
				break;
			case 27: //escape
			  hideMe();
			  break;
		}
		switch(event.keyCode) {
			case 13: // return
			    return false;
		}
	})
	.blur(function() {
		if ($container.is(':visible') && hasfocus > 0 ) {
			if(opt.debug) console.log('container visible and has focus')
		} else {
			//hideMe();	
		}
		inFocus=false;
	});
	
    $(document).mousedown(checkExternalClick);
    function checkExternalClick(event){
	    if ($(event.target).parents('.'+opt.containerClass).length === 0){
                hideMe();
	    }
    }
	function hideMe() { 
		hasfocus = 0;
		$container.hide();
		$('li.'+opt.hoverClass, $container).removeClass(opt.hoverClass);
		$('li.'+opt.currentClass, $container).addClass(opt.hoverClass);
	}
	
	function init() {
		$container.append(getSelectOptions($input.attr('id'))).hide();
		var width = ( $input.width() > $container.width() ) ? $input.width() : $container.width();
		if (width == 0 ) { width = $input.css('width').replace("px", ""); }
		
		var height = $input.height() + parseInt($input.css('paddingTop')) + parseInt($input.css('paddingBottom'));
		var position = $input.position();
		$container.css({width:width+"px", left:position.left, top:position.top+height});
    }
	
	function setupContainer(options) {
		var container = document.createElement("div");
		$container = $(container);
		$container.attr('id', elm_id+'_container');
		$container.addClass(options.containerClass);
		
		return $container;
	}
	
	function setupInput(options) {
		var input = document.createElement("input");
		var $input = $(input);
		$input.attr("id", elm_id+"_input");
		$input.attr("type", "text");
		$input.addClass(options.inputClass);
		$input.attr("autocomplete", "off");
		$input.attr("readonly", "readonly");
		$input.attr("tabIndex", $select.attr("tabindex")); // "I" capital is important for ie
	    $input.keydown(function(event) {	   
		    switch(event.keyCode) {
			    case 13: // return
				    event.preventDefault(); // seems not working in mac !
				    return false;
		    }
	    });
	    
		return $input;	
	}
	
	function moveSelect(step) {
		var lis = $("li", $container);
		if (!lis) return;

		active += step;

		if (active < 0) {
			active = 0;
		} else if (active >= lis.size()) {
			active = lis.size() - 1;
		}

		lis.removeClass(opt.hoverClass).removeClass(opt.currentClass);
		$(lis[active]).addClass(opt.hoverClass).addClass(opt.currentClass);

        scrollToActive(active);
        setCurrent(false);
	}
	function scrollToActive(liactive){
	    //todas as li's
		var lis = $("li", $container);
		//rolando sempre pro topo, pra comecar (TODO: consertar a logica pra nao precisar disso)
		$(lis[0]).parents('ul').scrollTop(0);
		//garante que a opcao ativa esteja no "range"
		if (liactive < 0) {
			liactive = 0;
		} else if (liactive >= lis.size()) {
			liactive = lis.size() - 1;
		}
		//distancia da opcao ate o topo
		var topoOp=$(lis[liactive]).position().top;
		//tamanho da opcao
		var tamanhoOp=$(lis[liactive]).height() + parseInt($(lis[liactive]).css('paddingTop').replace('px',''), 10)  + parseInt($(lis[liactive]).css('paddingBottom').replace('px',''), 10);
		//tamanho do container (ul)
		var tamanhoCont=$(lis[liactive]).parents('ul').height();
		//quanto ja rolou
		var rolagemCont=$(lis[liactive]).parents('ul').scrollTop();
		//se estiver fora da rolagem, abaixo
		if(topoOp+tamanhoOp >= tamanhoCont+rolagemCont){
            var rolarPara = (topoOp-tamanhoCont)+tamanhoOp+rolagemCont ;
		    $(lis[liactive]).parents('ul').scrollTop(rolarPara);
		}
		//se estiver fora da rolagem, acima
		if(topoOp < rolagemCont) $(lis[liactive]).parents('ul').scrollTop(topoOp);
	}
	
	function setCurrent(andHide) {
		var li = $("li."+opt.currentClass, $container).get(0);
		var ar = (''+li.id).split('_');
		var el = ar[ar.length-1];
		$select.val(el);
		$input.val($(li).html());
		if(andHide) $select.change();
		return true;
	}
	
	// select value
	function getCurrentSelected() {
		return $select.val();
	}
	
	// input value
	function getCurrentValue() {
		return $input.val();
	}
	
	function getSelectOptions(parentid) {
		var select_options = new Array();
		var ul = document.createElement('ul');
		$select.children('option').each(function() {
			var li = document.createElement('li');
			li.setAttribute('id', parentid + '_' + $(this).val());
			li.innerHTML = $(this).html();
			if ($(this).is(':selected')) {
				$input.val($(this).html());
				$(li).addClass(opt.currentClass);
			}
			ul.appendChild(li);
			$(li)
			.mouseover(function(event) {
				hasfocus = 1;
				if (opt.debug) console.log('over on : '+this.id);
				jQuery(event.target, $container).addClass(opt.hoverClass);
			})
			.mouseout(function(event) {
				hasfocus = -1;
				if (opt.debug) console.log('out on : '+this.id);
				jQuery(event.target, $container).removeClass(opt.hoverClass);
			})
			.click(function(event) {
			  	var fl = $('li.'+opt.hoverClass, $container).get(0);
				if (opt.debug) console.log('click on :'+this.id);
				$('li.'+opt.currentClass, $container).removeClass(opt.currentClass); 
				$(this).addClass(opt.currentClass);
				setCurrent(true);
				hideMe();
			});
		});
		return ul;
	}

    	
};
