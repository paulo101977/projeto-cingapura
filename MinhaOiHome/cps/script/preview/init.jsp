


var vui;

var vuiConfig = {
	apiPath: document.location.protocol+"//"+document.location.host+"/contentapi",
	appContext: document.location.protocol+"//"+document.location.host+"/content",
	cpsAppContext: document.location.protocol+"//"+document.location.host+"/cps",
	templatingAppContext: document.location.protocol+"//"+document.location.host+"/vgn-ext-templating",
	fileSourcePath: document.location.protocol+"//"+document.location.host+"/file_source"
};


var cpsConfig = {};
cpsConfig.isCPSConfigured = true;


var vcmConfig = {
};

var uiConfig = {
	appConsole: "/AppConsole"
};

function doPostSubmit(JSName, JSCall) {
	if (eval(JSName)) eval(JSCall);
}

(function(){
    var vgnScripts = [];
	
	
	vgnScripts.push(vuiConfig.apiPath+"/script/thirdparty/vquery/vquery.min.js");
    vgnScripts.push(vuiConfig.cpsAppContext+"/script/preview/vcm32266.js");
	vgnScripts.push(vuiConfig.apiPath+"/script/thirdparty/vquery/vquery.form.js");
	vgnScripts.push(vuiConfig.cpsAppContext+"/script/preview/vquery-ux.js");

	
	if (typeof vuit === 'undefined') {
		vgnScripts.push(vuiConfig.appContext+"/script/thirdparty/vuit/vuit.js");
	}

	
	if (typeof vExt === 'undefined') {
		vgnScripts.push(vuiConfig.appContext+"/script/thirdparty/vext/adapter/vext/vext-base.js");
		vgnScripts.push(vuiConfig.appContext+"/script/thirdparty/vext/vext-all.js");
	}
	if (typeof vExt === 'undefined' || typeof vExt.ux === 'undefined') {
		vgnScripts.push(vuiConfig.appContext+"/script/thirdparty/vext/vext-ux/vext-ux.js");
	}

	
	
	vgnScripts.push(vuiConfig.apiPath+"/script/impl/vquery/logging.js");
	vgnScripts.push(vuiConfig.apiPath+"/script/impl/vquery/vui-impl.js");
	vgnScripts.push(vuiConfig.apiPath+"/script/vui/vui.js");

	
	vgnScripts.push(vuiConfig.appContext+"/script/vui/ui/vui-ui.js");
	vgnScripts.push(vuiConfig.appContext+"/script/vui/ui/vext.js");
	vgnScripts.push(vuiConfig.cpsAppContext+"/script/preview/vext-ux.js");

	
	vgnScripts.push(vuiConfig.apiPath+"/script/vcm/vcm.js");

	
	vgnScripts.push(vuiConfig.appContext+"/script/vcm/ui/vcm-ui.js");

	
	vgnScripts.push(vuiConfig.appContext+"/script/messages.jsp");
	vgnScripts.push(vuiConfig.cpsAppContext+"/script/common/messages.jsp");
	vgnScripts.push(vuiConfig.cpsAppContext+"/script/preview/messages.jsp");

	
	vgnScripts.push("/editLiveJava/editlivejava.js");
	vgnScripts.push("/editLiveJava/inlineEditing.js");

	
	vgnScripts.push(vuiConfig.cpsAppContext+"/script/cps-preview-all.js");

	


	var vgnScriptsBlock = "";
	for(var i=0; i<vgnScripts.length; i++){
		var vgnScript = vgnScripts[i];
		vgnScriptsBlock += "<script type='text/javascript' src='" + vgnScript + "'></script>";
	}
	document.write(vgnScriptsBlock);
})();
