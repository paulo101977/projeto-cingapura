var gulp = require('gulp'),
    serve = require('gulp-serve'),
    clean = require('gulp-clean'),
    sass = require('gulp-sass'),
    runSequence = require('run-sequence'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    inject = require('gulp-inject-string'),
    rename = require('gulp-rename'), //rename files
    filenames = require("gulp-filenames"), //read file names
    through2 = require('through2'),
    foreach = require('gulp-foreach'),
    path = require('path'),
    shell = require('gulp-shell'), //exec shell
    fs = require("fs"); //read files

  const SET_HEROKU = "cd dist;"
                      + "git init;"
                      + "git add .;"
                      + "git commit -m 'Update heroku';"
                      + "git remote add heroku https://git.heroku.com/cingapura.git"

  const ZIP_FILE = "zip --password 123 -r dist.zip dist/"

  //set git heroku to deploy
  gulp.task('set:heroku', shell.task([SET_HEROKU]))

  //zip the dist folder
  gulp.task('zip:dist', shell.task([ZIP_FILE]))


  gulp.task('copy:php', function(){
    return gulp.src('./index.php')
            .pipe(gulp.dest('./tmp'))
  })

  //copy files
  gulp.task('copy:to:tmp', function(){
    return gulp.src('./MinhaOiHome/**/*.*')
            .pipe(gulp.dest('./tmp'))
  })

  //copy js files
  gulp.task('copy:js:to:tmp', function(){
    return gulp.src('./js/**/*.*')
            .pipe(gulp.dest('./tmp/js'))
  })

  gulp.task('copy:to:dist', function(){
    return gulp.src('./tmp/**/*.*')
            .pipe(gulp.dest('./dist'))
  })

  gulp.task('sass', function () {
    return gulp.src('./scss/main.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(
        autoprefixer({
              browsers: ['last 2 versions'],
              cascade: false
        }))
      .pipe(gulp.dest('tmp/css'))
      .pipe(browserSync.stream());
  });

  //clean
  gulp.task('clean:dist', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
  });

  gulp.task('clean:tmp', function () {
    return gulp.src('tmp', {read: false})
        .pipe(clean());
  });

  gulp.task('clean', function (done) {
    return runSequence(
      //'clean:dist',
      'clean:tmp',
      function(){
        done();
      }
    )
  });


  //inject
  gulp.task('copy:css' , function(){
    return gulp.src('./tmp/css/main.css')
        .pipe(gulp.dest('./dist/route/css/main.css'))
  })

  gulp.task('copy:js' , function(){
    // /* <!-- inject:js --> */
    return gulp.src('./tmp/js/**/*.js')
        .pipe(gulp.dest('./dist/js/'))
  })


  gulp.task('inject:templates', function(done){
    return gulp.src('./templates/*.html')
      .pipe(
        foreach(function(stream, file){
          //var file = file.path.split('/')[file.path.split('/').length-1]
          var name = path.basename(file.path);
          var fileContent = fs.readFileSync("./templates/" + name, "utf8");

          return gulp
            .src('./tmp/index.html')
            .pipe(
              inject.before(
                '<div id="home-inicio" class="main pull-left">',
                  fileContent
              )
            )
            .pipe(rename(name)) //rename index.html to file name inside ./templates
        })
      )
      //.pipe(gulp.dest('./tmp/route/'));
      .pipe(gulp.dest('./tmp/'));
  })

  //inject links at index.html
  gulp.task('inject:links' , function(){

    return gulp.src('./templates/*.html')
      .pipe(
        foreach(function(stream, file){
          //var file = file.path.split('/')[file.path.split('/').length-1]
          var name = path.basename(file.path);
          var fileContent = "<b>"
                              + "<a target='_blank' href='./"+ name +"'>" + name + "</a>"
                            + "</b>"
                            + "<br />"

          return gulp
            .src('./tmp/index.html')
            .pipe(
              inject.after(
                '<div id="home-inicio" class="main pull-left">',
                  fileContent
              )
            )
            .pipe(gulp.dest('./tmp/'))
        })
      )
      //.pipe(gulp.dest('./tmp/'));
  })

  gulp.task('inject:sidebar', function(){
    var fileContent = fs.readFileSync("./sidebar/sidebar.html", "utf8");

    return gulp
              .src('./tmp/index.html')
              .pipe(inject.after('<div class="col-xs-3 sidebar pull-left">' , fileContent))
              .pipe(gulp.dest('./tmp'));
  })

  gulp.task('watch' , function(done){
    return gulp.watch(
      [
        'js/**/*.*',
        'scss/**/*.*',
        'templates/**/*.*',
        'MinhaOiHome/**/*.*'
      ]
    )
    .on('change', function(){
      //browserSync.reload();
      runSequence(
        'build',
        'reload',
        'clean:tmp',
        'set:heroku',
        function(){
          done();
        }
      )
    })
  })

  gulp.task('reload', function(){
    setTimeout(function () {
      browserSync.reload();
    }, 500);
  })

  // Static server
  gulp.task('browser-sync', function() {
      return browserSync.init({
          server: {
              baseDir: "./dist"
          },
          port: 8080

      });
  });

  //build
  gulp.task('build', function(done){
    return runSequence(
      'clean:dist',
      'copy:php',
      'sass',
      'copy:to:tmp',
      'copy:js:to:tmp',
      'inject:sidebar',
      'inject:templates',
      'copy:js',
      'copy:css',
      'inject:links', //inject to navigate
      'copy:to:dist',
      'zip:dist',
      //'clean:tmp',
      function(){
        done();
      }
    )
  })

  gulp.task('build:dist', function(done){
    return runSequence(
      'clean',
      'build',
      //'build',
      'clean:tmp',
      'set:heroku',
      function(){
        done();
      }
     )
  })


  gulp.task('serve', function(done){
    return runSequence(
      'clean',
      'build',
      'set:heroku',
      'browser-sync',
      'watch',
      function(){
        done();
      }
     )
  })
